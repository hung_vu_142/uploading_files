import os
import sys
import logging
# We'll render HTML templates and access data sent by POST
# using the request object from flask. Redirect and url_for
# will be used to redirect the user once the upload is done
# and send_from_directory will help us to send/show on the
# browser the file that the user just uploaded
from flask import Flask, render_template, request, redirect, url_for, send_from_directory
from werkzeug import secure_filename

import xlrd
from openpyxl import Workbook as OpenpyxlWorkbook
from openpyxl import load_workbook

# Initialize the Flask application
app = Flask(__name__)


# This is the path to the upload directory
app.config['UPLOAD_FOLDER'] = '/tmp/'
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['xls', 'xlsx'])
# This is the path to the upload directory
app.config['DOWNLOAD_FOLDER'] = '/tmp/'

# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

def clean_dir(folder):
    for file in os.listdir(folder):
        file_path = os.path.join(folder, file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
        except Exception as e:
            print e

def convert_xls_to_xlsx(xls_path, output_path):
    '''
    Converts a given xls to xlsx while maintaining the structure of the
    original file.
    '''

    xls_file = xlrd.open_workbook(xls_path)

    fresh_workbook = OpenpyxlWorkbook()

    # Iterate over all the sheets in the workbook.
    # nsheets contains the count of sheets in the original file.
    for i in range(xls_file.nsheets):
        # Retrieve the sheet to be converted.
        xls_sheet = xls_file.sheet_by_index(i)

        # If we are converting the first sheet, take the active blank one
        # of the target wb, if not create a new one.
        if i == 0:
            target_sheet = fresh_workbook.active
        else:
            target_sheet = fresh_workbook.create_sheet()

        # Copy the name of the sheet to the blank one.
        target_sheet.title = xls_sheet.name

        # Iterate over all rows in the sheet.
        for row in range(xls_sheet.nrows):
            # Iterate through all columns inside the row.
            for column in range(xls_sheet.ncols):
                # Create a new cell with the exact same context as the old
                # one.
                target_sheet.cell(
                    row=row+1,
                    column=column+1,
                ).value = xls_sheet.cell_value(row, column)
                
    xlsx_name = os.path.basename(xls_path).lower()
    xlsx_path = os.path.join(output_path, xlsx_name).replace('.xls', '.xlsx')

    fresh_workbook.save(xlsx_path)
    return xlsx_path

# This route will show a form to perform an AJAX request
# jQuery is loaded to execute the request and update the
# value of the operation
@app.route('/')
def index():
    clean_dir(app.config['UPLOAD_FOLDER'])
    clean_dir(app.config['DOWNLOAD_FOLDER'])
    return render_template('index.html')


# Route that will process the file upload
@app.route('/upload', methods=['POST'])
def upload():
    # Get the name of the uploaded files
    uploaded_files = request.files.getlist("file[]")
    filenames = []
    for file in uploaded_files:
        # Check if the file is one of the allowed types/extensions
        if file and allowed_file(file.filename):
            # Make the filename safe, remove unsupported chars
            filename = secure_filename(file.filename)
            # Move the file form the temporal folder to the upload
            # folder we setup
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            # Save the filename into a list, we'll use it later
    
    filenames = os.listdir(app.config['UPLOAD_FOLDER'])
            # Load an html page with a link to each uploaded file
    return render_template('upload.html', filenames=filenames)

# This route is expecting a parameter containing the name
# of a file. Then it will locate that file on the upload
# directory and show it on the browser, so if the user uploads
# an image, that image is going to be show after the upload
@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'], filename)

@app.route('/convert', methods=['POST'])
def convert():
    outputs = []
    input_files = os.listdir(app.config['UPLOAD_FOLDER'])

    for input_file in input_files:
        input_path = os.path.join(app.config['UPLOAD_FOLDER'], input_file)
        if input_path.endswith('xls'):
            output_path = convert_xls_to_xlsx(
                input_path, 
                app.config['DOWNLOAD_FOLDER']
            )
        elif input_path.endswith('xlsx'):
            wb = load_workbook(input_path)
            output_name = os.path.basename(input_path).lower()
            output_path = os.path.join(app.config['DOWNLOAD_FOLDER'], output_name)
            wb.save(output_path)
        output = os.path.basename(output_path)
        outputs.append(output)
    return render_template('convert.html', outputs=outputs)

@app.route('/converted/<output>')
def converted_file(output):
    return send_from_directory(app.config['DOWNLOAD_FOLDER'], output)


# Create log file on heroku server.
app.logger.addHandler(logging.StreamHandler(sys.stdout))
app.logger.setLevel(logging.ERROR)